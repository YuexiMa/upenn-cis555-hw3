package edu.upenn.cis455.mapreduce;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import edu.upenn.cis455.mapreduce.master.WorkerStatus;

/**
 * A class used to store map-reduce job parameters, as well as provide a send POST function.
 * The corresponding parse functions can be used by master, map worker, as well as reduce worker.
 * @author yuexi
 *
 */
public class JobInfo{
	
	public String classname;
	public String inputdir;
	public String outputdir;
	public int nthreadmap;
	public int nthreadreduce;
	public int nthread;
	private String params;
	
	
	public int nWorkers;
	public String[] workerUrls;
	
	public JobType jobType;
	
	enum JobType{
		master, map, reduce, error;
	}

	/**
	 * Master should call this function.
	 * @param request
	 */
	public void parseFormRequest(HttpServletRequest request){
		this.classname = request.getParameter("classname");
		this.inputdir = request.getParameter("inputdir");
		this.outputdir = request.getParameter("outputdir");
		this.nthreadmap = Integer.parseInt(request.getParameter("nthreadmap"));
		this.nthreadreduce = Integer.parseInt(request.getParameter("nthreadreduce"));
		this.jobType = JobType.master;
	}
	
	/**
	 * Worker doing map job should call this function.
	 * @param request
	 */
	public void parseMapRequest(HttpServletRequest request){
		this.classname = request.getParameter("job");
		this.inputdir = request.getParameter("input");
		this.nthread = Integer.parseInt(request.getParameter("numThreads"));
		this.nWorkers = Integer.parseInt(request.getParameter("numWorkers"));
		
		this.workerUrls = new String[this.nWorkers];
		for(int i=0; i<this.nWorkers; i++){
			this.workerUrls[i] = request.getParameter("worker"+(i+1));
		}
		this.jobType = JobType.map;
	}
	
	/**
	 * Worker doing reduce job should call this function.
	 * @param request
	 */
	public void parseReduceRequest(HttpServletRequest request){
		this.classname = request.getParameter("job");
		this.outputdir = request.getParameter("output");
		this.nthread = Integer.parseInt(request.getParameter("numThreads"));

		this.jobType = JobType.reduce;
	}
	

	public void setParams(String params) {
		this.params = params;
	}

	public void sendPostTo(WorkerStatus w, String uri) {
		HttpURLConnection conn = null;
		try{
			URL url = new URL("http://"+w.ip+":"+w.port+uri);
			conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("POST");
			conn.connect();
			
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
			out.write(this.params);
			out.flush();
			out.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(conn!=null) conn.disconnect();
		}
	}
	
}