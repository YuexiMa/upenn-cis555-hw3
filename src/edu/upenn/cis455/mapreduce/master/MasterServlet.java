package edu.upenn.cis455.mapreduce.master;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.upenn.cis455.mapreduce.JobInfo;

/**
 * Master servlet for map-reduce.
 * @author Yuexi Ma
 *
 */
public class MasterServlet extends HttpServlet {

	static final long serialVersionUID = 455555001;
	
	private LinkedList<JobInfo> jobQueue = new LinkedList<>();
	private JobInfo jobActive = null;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws java.io.IOException
	{
		switch(request.getRequestURI()){
		case "/workerstatus":
			this.handleWorkerStatus(request, response);
			this.checkJobStatus();
			break;
		case "/status":
			this.handleStatus(request, response);
			break;
		default:
		}
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws java.io.IOException
	{
		switch(request.getRequestURI()){
		case "/submitjob":
			this.handleSubmitJob(request, response);
			break;
		default:
		}
	}

	/**
	 * handle user's new job submission.
	 * @param request
	 * @param response
	 */
	private void handleSubmitJob(HttpServletRequest request,
			HttpServletResponse response) {
		JobInfo js = new JobInfo();
		js.parseFormRequest(request);
		
		this.jobQueue.addLast(js);
	}
	
	/**
	 * send map job to workers
	 * @param js
	 */
	private synchronized void sendJobRunMap(JobInfo js){
		ArrayList<WorkerStatus> activeWorkers = this.getActiveWorkers();

		HttpParameterBuilder hpb = new HttpParameterBuilder();
		hpb.addParam("job", js.classname);
		hpb.addParam("input", js.inputdir);
		hpb.addParam("numThreads", js.nthreadmap);
		hpb.addParam("numWorkers", activeWorkers.size());
		for(int i=0; i<activeWorkers.size(); i++){
			hpb.addParam("worker"+(i+1),activeWorkers.get(i).ip+":"+activeWorkers.get(i).port);
		}
		
		js.setParams(hpb.build());
		
		for(WorkerStatus w: activeWorkers){
			js.sendPostTo(w, "/runmap");
		}
	}
	
	/**
	 * send reduce jobs to workers.
	 * @param js
	 */
	private synchronized void sendJobRunReduce(JobInfo js){
		ArrayList<WorkerStatus> activeWorkers = this.getActiveWorkers();

		HttpParameterBuilder hpb = new HttpParameterBuilder();
		hpb.addParam("job", js.classname);
		hpb.addParam("output", js.outputdir);
		hpb.addParam("numThreads", js.nthreadmap);
		
		js.setParams(hpb.build());
		
		for(WorkerStatus w: activeWorkers){
			js.sendPostTo(w, "/runreduce");
		}
	}
	
	/**
	 * check job status
	 */
	private synchronized void checkJobStatus(){
		if(this.jobActive == null){
			if(this.jobQueue.isEmpty()==false){
				this.jobActive = this.jobQueue.removeFirst();
				this.sendJobRunMap(this.jobActive);
			}
		}else if(this.checkActiveWorkersStatus("waiting")){
			this.sendJobRunReduce(this.jobActive);
		}else if(this.checkActiveWorkersStatus("idle")){
			this.jobActive = null;
		}
	}
	
	private boolean checkActiveWorkersStatus(String status){
		for(WorkerStatus w: this.getActiveWorkers()){
			if(w.status.equals(status)==false){
				return false;
			}
		}
		
		return true;
	}
	
	
	
	/**
	 * generate worker status page
	 * @param request
	 * @param response
	 * @throws IOException
	 */
 	private void handleStatus(HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		HtmlTableBuilder htb = new HtmlTableBuilder();
		htb.addRow("","","Workers status","","");
		htb.addRow("Port","Status","Job","Keys read", "Keys written");
		
		for(WorkerStatus w: this.getActiveWorkers()){
			htb.addRow(w.ip+":"+w.port,w.status,w.job,w.keysRead,w.keysWritten);
		}
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.write(HtmlGenerator.wrapInHtmlBody("Name: Yuexi Ma<br>PennKey: yuexi<br><br><br>"
												+ htb.build()));
		
		out.write("<br><br><br><br><br><br>");
		out.write(this.generateHtmlJobSubmissionForm());
		
		out.flush();
		out.close();
	}
	
	private String generateHtmlJobSubmissionForm(){
		return 
				"<form action=\"submitjob\" method=\"get\">"+
				"  Class name of the job:<br><input type=\"text\" name=\"classname\" value=\"\">"+
				"  <br>Input directory:<br><input type=\"text\" name=\"inputdir\" value=\"\">"+
				"  <br>Output directory:<br><input type=\"text\" name=\"outputdir\" value=\"\">"+
				"  <br>Number of map threads:<br><input type=\"text\" name=\"nthreadmap\" value=\"\">"+
				"  <br>Number of reduce threads:<br><input type=\"text\" name=\"nthreadreduce\" value=\"\">"+
				"  <br><br>"+
				"  <input type=\"submit\" value=\"Submit\">"+
				"</form>";
	}

	private ArrayList<WorkerStatus> getActiveWorkers(){
		ArrayList<WorkerStatus> res = new ArrayList<>();
		
		long now = System.currentTimeMillis();
		for(WorkerStatus w: this.workerStatusTable.values()){
			if(now - w.lastUpdate < 30 * 1000){
				res.add(w);
			}
		}
		
		return res;
	}

	
	
	
	/**
	 * handle worker status information
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void handleWorkerStatus(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String ip = request.getLocalAddr();
		String port = request.getParameter("port");
		
		WorkerStatus ws = workerStatusTable.get(port+":"+ip);
		if(ws==null){
			ws=new WorkerStatus();
			workerStatusTable.put(port+":"+ip, ws);
		}
		ws.update(request);
	}

	HashMap<String,WorkerStatus> workerStatusTable = new HashMap<>();


}





