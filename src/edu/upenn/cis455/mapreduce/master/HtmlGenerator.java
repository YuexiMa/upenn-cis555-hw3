package edu.upenn.cis455.mapreduce.master;

/**
 * Html string generator.
 * @author yuexi
 *
 */
public class HtmlGenerator{
	
	/**
	 * wrap string in html body.
	 * @param s
	 * @return
	 */
	public static String wrapInHtmlBody(String s){
		return "<!DOCTYPE html><html><body>"+s+"</body></html>";
	}
}