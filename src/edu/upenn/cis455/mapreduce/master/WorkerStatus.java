package edu.upenn.cis455.mapreduce.master;

import javax.servlet.http.HttpServletRequest;

/**
 * A class used by map-reduce master to record map-reduce worker status.
 * @author Yuexi Ma
 *
 */
public class WorkerStatus{
	public String ip;
	public String port;
	public String status;
	public String job;
	public String keysRead;
	public String keysWritten;
	public long lastUpdate;

	/**
	 * update worker status
	 * @param request
	 */
	public void update(HttpServletRequest request){
		this.ip = request.getLocalAddr();
		this.port = request.getParameter("port");
		this.status = request.getParameter("status");
		this.job = request.getParameter("job");
		this.keysRead = request.getParameter("keysRead");
		this.keysWritten = request.getParameter("keysWritten");
		this.lastUpdate = System.currentTimeMillis();
	}
	
}