package edu.upenn.cis455.mapreduce.master;

/**
 * Http parameter string generator
 * @author cis455
 *
 */
public class HttpParameterBuilder{
	StringBuilder sb=null;
	
	/**
	 * add parameter
	 * @param name
	 * @param value
	 */
	public void addParam(String name, int value){
		this.addParam(name, ""+value);
	}
	
	/**
	 * add parameter
	 * @param name
	 * @param value
	 */
	public void addParam(String name, String value){
		if(sb==null){
			sb = new StringBuilder();
			sb.append(name+"="+value);
		}else{
			sb.append("&"+name+"="+value);
		}
	}
	
	/**
	 * generate parameter string and clear memory.
	 * @return
	 */
	public String build(){
		try{
			return sb.toString();
		}finally{
			sb = null;
		}
	}
}
