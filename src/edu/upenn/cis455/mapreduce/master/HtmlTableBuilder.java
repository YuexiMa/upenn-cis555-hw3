package edu.upenn.cis455.mapreduce.master;

/**
 * Html table builder
 * @author yuexi
 *
 */
public class HtmlTableBuilder{
	StringBuilder sb=null;
	
	/**
	 * add an item in table
	 * @param items
	 */
	public void addRow(String... items){
		if(sb==null){
			sb = new StringBuilder();
			sb.append("<table style=\"width:100%\">");
		}
		sb.append("<tr>");
		for(String item: items){
			sb.append("<td>"+item+"</td>");
		}
		sb.append("</tr>");
	}
	
	/**
	 * generate the table string and clear the storage.
	 * @return the html table string
	 */
	public String build(){
		sb.append("<\table>");
		try{
			return sb.toString();
		}finally{
			sb = null;
		}
	}
}