package edu.upenn.cis455.mapreduce.worker;

/**
 * Map reduce worker servlet status
 * @author Yuexi Ma
 *
 */
public enum Status {
	mapping, waiting, reducing, idle;
}
