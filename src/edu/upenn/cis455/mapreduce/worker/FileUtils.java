package edu.upenn.cis455.mapreduce.worker;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * File utility class
 * @author yuexi
 *
 */
public class FileUtils {
	
	/**
	 * Remove directory and sub-files. This does not remove sub-directories.
	 * @param dir
	 */
	public static void rmdir(File dir){
		if(dir.exists() && dir.isDirectory()){
			for(File f: dir.listFiles()){
				f.delete();
			}
			
			dir.delete();
		}
	}
	
	/**
	 * Append string content to a file.
	 * @param dir
	 * @param filename
	 * @param content
	 */
	public static synchronized void appendToFile(String dir, String filename, String content){
		try (BufferedWriter br = new BufferedWriter(new FileWriter(new File(dir,filename), true));){
			br.write(content+" ");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
