package edu.upenn.cis455.mapreduce.worker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.upenn.cis455.mapreduce.Context;
import edu.upenn.cis455.mapreduce.Job;
import edu.upenn.cis455.mapreduce.JobInfo;
import edu.upenn.cis455.mapreduce.master.HttpParameterBuilder;

/**
 * Worker servlet for map-reduce.
 * @author Yuexi Ma
 *
 */
public class WorkerServlet extends HttpServlet {

	static final long serialVersionUID = 455555002;

	private String 				masterUrl;
	private Timer  				timerStatusReport;
	private String 				port;
	private String 				storagedir;
	
	private JobInfo 			jobInfo   			= new JobInfo();
	private Status  			status 				= Status.idle;
	
	private int 				keysRead 			= 0;
	private int 				keysWritten 		= 0;
	
	private Job 				mapReduceJobWorker;
	private ThreadPoolExecutor 	threadPool;
	
	private MapEmitHandler		mapEmitHandler;
	private ReduceEmitHandler	reduceEmitHandler;
	
	
	@Override
	public void init(ServletConfig config){
		this.port 		= config.getInitParameter("port");
		this.masterUrl  = config.getInitParameter("master");
		this.storagedir = config.getInitParameter("storagedir");
		
		this.initStatusReportThread();
	}
	
	/**
	 * Should be called only once.
	 */
	private void initStatusReportThread() {
		TimerTask jobReport = new TimerTask(){
			@Override
			public void run() {
				sendStatus();
			}
		};
			
		this.timerStatusReport = new Timer();
		this.timerStatusReport.schedule(jobReport, 1*1000, 10*1000);
	}

	/**
	 * send worker status to master
	 */
	private synchronized void sendStatus(){
		HttpURLConnection conn = null;
		try{
			HttpParameterBuilder hpb = new HttpParameterBuilder();
			hpb.addParam("port", 		this.port);
			hpb.addParam("status",		this.status.toString());
			hpb.addParam("job", 		this.jobInfo.classname);
			hpb.addParam("keysRead", 	this.keysRead+"");
			hpb.addParam("keysWritten", this.keysWritten+"");
			
			URL url = new URL("http://"+this.masterUrl+"/workerstatus"+"?"+hpb.build());
			conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("GET");
			conn.connect();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(conn!=null) conn.disconnect();
		}
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws java.io.IOException
	{

	}


	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws java.io.IOException
	{
		switch(request.getRequestURI()){
		case "/runmap":
			this.handleRunMap(request, response);
			break;
		case "/runreduce":
			this.handleRunReduce(request, response);
			break;
		case "/pushdata":
			this.handlePushData(request, response);
			break;
		default:
		}
	}

	/**
	 * handlePushData
	 * @param request
	 * @param response
	 */
	private void handlePushData(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			this.writeSpoolIn(HttpServletRequestReader.getBody(request));
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				request.getReader().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private synchronized void writeSpoolIn(String content) {
		FileUtils.appendToFile(this.storagedir+"/spool-in","content.txt", content);
	}

	/**
	 * handle map job
	 * @param request
	 * @param response
	 */
	private void handleRunMap(HttpServletRequest request,
		HttpServletResponse response) {
		
		this.status = Status.mapping;
		
		this.jobInfo.parseMapRequest(request);
		
		this.initMapReduceThreads();
		
		this.prepareSpoolDirectories();

		this.mapEmitHandler = new MapEmitHandler();
		
		this.startMap();
		
		this.shutdownThreadPool();
		
		this.mapEmitHandler.closeAll();
		this.mapEmitHandler = null;
		
		this.pushData();
		
		this.status = Status.waiting;
		
		this.sendStatus();
	}
	
	private void shutdownThreadPool(){
		this.threadPool.shutdown();
		try {
			this.threadPool.awaitTermination(24l, TimeUnit.HOURS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.threadPool = null;
	}
	
	/**
	 * push map result to workers
	 */
	private void pushData() {
		File dir = new File(this.storagedir, "spool-out");
		for(File f: dir.listFiles()){
			HttpURLConnection conn = null;
			try{
				URL url = new URL("http://"+f.getName().replace('_', ':')+"/pushdata");
				conn = (HttpURLConnection)url.openConnection();
				conn.setRequestMethod("POST");
				conn.connect();
				
				BufferedWriter out = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
				out.write(new String(Files.readAllBytes(f.toPath())).toCharArray());
				out.flush();
				out.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(conn!=null) conn.disconnect();
			}
		}
	}

	private void prepareSpoolDirectories(){
		File spoolOut = new File(this.storagedir,"spool-out");
		File spoolIn  = new File(this.storagedir,"spool-in");
		
		FileUtils.rmdir(spoolOut);
		FileUtils.rmdir(spoolIn);
		
		spoolOut.mkdirs();
		spoolIn.mkdirs();
	}
	
	/**
	 * start map job
	 */
	private void startMap() {
		File dir = new File(this.storagedir, this.jobInfo.inputdir);
		for(File f: dir.listFiles()){
			try (BufferedReader fr = new BufferedReader(new FileReader(f));){
				String line;
				while((line=fr.readLine())!=null){
					final int idx = line.indexOf('\t');
					this.assignMap(line.substring(0, idx), line.substring(idx+1));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void assignMap(final String key, final String value){
		this.threadPool.execute(new Runnable(){
			@Override
			public void run() {
				mapReduceJobWorker.map(key, value, mapEmitHandler);
			}
		});
	}

	private void initMapReduceThreads(){
		try {
			this.mapReduceJobWorker = (Job) Class.forName(this.jobInfo.classname).newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.threadPool = new ThreadPoolExecutor(
								this.jobInfo.nthread, 
								this.jobInfo.nthread, 
								1000*10,
								TimeUnit.MILLISECONDS, 
								new LinkedBlockingQueue<Runnable>()
								);
	}
	
	/**
	 * handle reduce job
	 * @param request
	 * @param response
	 */
	private void handleRunReduce(HttpServletRequest request,
			HttpServletResponse response) {
		this.status = Status.reducing;
		
		this.jobInfo.parseReduceRequest(request);
		
		this.reduceEmitHandler = new ReduceEmitHandler();
		
		this.initMapReduceThreads();
		
		this.sortSpoolInFile();
		
		this.startReduce();
		
		this.shutdownThreadPool();
		
		this.reduceEmitHandler.closeAll();
		this.reduceEmitHandler = null;
		
		this.status = Status.idle;
		
		this.sendStatus();
	}

	/**
	 * start reduce job
	 */
	private void startReduce() {
		File f = new File(this.storagedir+"/spool-in","/content.txt");
		
		try (BufferedReader fr = new BufferedReader(new FileReader(f));){
			String key = null;
			ArrayList<String> values = new ArrayList<>();
			
			String line;
			
			while((line = fr.readLine())!=null){
				final String[] kvPair = line.split("\t");
				
				if(kvPair.length!=2){
					continue;
				}
		
				if(key==null){
					key = kvPair[0];
				}else if(key.equals(kvPair[0])==false){
					this.assignReduce(key, values.toArray(new String[0]));
					
					values = new ArrayList<>();
					key = kvPair[0];
				}
				
				values.add(kvPair[1]);
			}
			
			if(key!=null){
				this.assignReduce(key, values.toArray(new String[0]));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void assignReduce(final String key, final String[] values){
		this.threadPool.execute(new Runnable(){
			@Override
			public void run() {
				mapReduceJobWorker.reduce(key, values, reduceEmitHandler);
			}
		});
	}

	/**
	 * sort spool-in files using Unix sort command
	 */
	private void sortSpoolInFile(){
		try {
			File f = new File(this.storagedir+"/spool-in","/content.txt");
			Process p = Runtime.getRuntime().exec("sort -o "+f.getAbsolutePath()+" "+f.getAbsolutePath());
			p.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Emit handler class
	 * @author Yuexi
	 *
	 */
	protected class ReduceEmitHandler implements Context{
		private BufferedWriter br = null;
		
		@Override
		public void write(String key, String value) {
			this.appendToFile(storagedir+"/"+jobInfo.outputdir, "result.txt", key+"\t"+value+"\n");
		}

		private synchronized void appendToFile(String dir, String filename, String content){
			if(br == null){
				try {
					br = new BufferedWriter(new FileWriter(new File(dir,filename), true));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			try {
				br.write(content+" ");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void closeAll(){
			try {
				br.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
	}
	
	protected class MapEmitHandler implements Context{
		
		private HashMap<String, BufferedWriter> writers = new HashMap<>();
		
		@Override
		public void write(String key, String value) {
			int idx = Hash.mapDataIntoRange(key, jobInfo.nWorkers);
			String filename = jobInfo.workerUrls[idx].replace(':', '_');
			this.appendToFile(storagedir+"/spool-out", filename, key+"\t"+value+"\n");
		}
		
		private synchronized void appendToFile(String dir, String filename, String content){
			BufferedWriter br = this.writers.get(filename);
			if(br == null){
				try {
					br = new BufferedWriter(new FileWriter(new File(dir,filename), true));
					this.writers.put(filename, br);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			try {
				br.write(content+" ");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		public void closeAll(){
			for(BufferedWriter br: this.writers.values()){
				try {
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
	}

}

