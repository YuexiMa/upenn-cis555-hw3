package edu.upenn.cis455.mapreduce.worker;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Hash calculation and hash space mapping class
 * @author yuexi
 *
 */
public class Hash {

	public static int mapHashSHA1IntoRange(String data, int range){
		return mapHashSHA1IntoRange(data.getBytes(), range);
	}
	
	public static int mapHashSHA1IntoRange(byte[] data, int range){
		BigInteger bi = new BigInteger(data);
		return bi.mod(new BigInteger(range+"")).intValue();
	}
	
	public static int mapDataIntoRange(String data, int range){
		return mapHashSHA1IntoRange(data.getBytes(), range);
	}
	
	public static int mapDataIntoRange(byte[] data, int range){
		return mapHashSHA1IntoRange(hashSHA1(data), range);
	}
	
	public static String hashSHA1String(String data){
		return new String(hashSHA1(data));
	}
	
	public static String hashSHA1String(byte[] data){
		return new String(hashSHA1(data));
	}
	
	public static byte[] hashSHA1(String data){
		return hashSHA1(data.getBytes());
	}
	
	public static byte[] hashSHA1(byte[] data){
		MessageDigest mDigest;
		try {
			mDigest = MessageDigest.getInstance("SHA1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
		return mDigest.digest(data);
	}
}
