package edu.upenn.cis455.mapreduce.worker;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

/**
 * A class used to read Http request information.
 * @author Yuexi Ma
 *
 */
public class HttpServletRequestReader {
	public static String getBody(HttpServletRequest request) throws IOException{
		BufferedReader reader = request.getReader();

		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		
		int b = reader.read();
		while(b!=-1){
			bo.write(b);
		}
		
		return bo.toString();
	}
}
