package edu.upenn.cis455.mapreduce.job;

import edu.upenn.cis455.mapreduce.Context;
import edu.upenn.cis455.mapreduce.Job;

/**
 * A map-reduce job for counting words in documents.
 * @author yuexi
 *
 */
public class WordCount implements Job {

  public void map(String key, String value, Context context)
  {
    String[] words = value.split("\\s+");
    for(String w: words){
    	context.write(w, "1");
    }
  }
  
  public void reduce(String key, String[] values, Context context)
  {
    context.write(key, values.length+"");
  }
  
}
